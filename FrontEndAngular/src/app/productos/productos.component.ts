import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiciosService } from '../persistencia/servicios.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  productos: any = [];

  constructor(private servicios: ServiciosService, private router: Router) { }

  ngOnInit(): void {
    this.servicios.getProductos().subscribe(result => {
      this.productos = result;
      console.log(result);
    },
      error => {
        console.log(error);
      }
    );
  }

}
