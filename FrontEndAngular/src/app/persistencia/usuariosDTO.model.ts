export class usuariosDTO {
    id: number;
    nombre: string;
    identificacion: string;
    direccion: string;
    telefono: string;
    email: string;
    login: string;
    clave: string;
    fechacreacion: string;
}

export class productosDTO {
    id: number;
    titulo: string;
    descripcion: string;
    imagen: string;
    estado: number;
    usuariocreacion: string;
    fechacreacion: string;
}