import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {

  constructor(private httpClient: HttpClient) { }

  autenticarUsuario(data: any): Observable<any> {
    let headers = new HttpHeaders()
      .set('Content-Type', 'application/json');
    return this.httpClient.post<any>("http://localhost:8080/usuarios/login", data, { headers: headers });
  }

  getProductos(): Observable<any> {
    let headers = new HttpHeaders()
      .set('Content-Type', 'application/json');
    return this.httpClient.get<any>("http://localhost:8080/productos/get", { headers: headers });
  }

  setProducto(data: any): Observable<any> {
    let headers = new HttpHeaders()
      .set('Content-Type', 'application/json');
    return this.httpClient.post<any>("http://localhost:8080/productos/set", data, { headers: headers });
  }
}
