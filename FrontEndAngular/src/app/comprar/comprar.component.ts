import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-comprar',
  templateUrl: './comprar.component.html',
  styleUrls: ['./comprar.component.css']
})
export class ComprarComponent implements OnInit {

  errorLabel = false;
  successLabel = false;
  errorText: string;
  successText: string;
  producto: string;

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.producto = this.route.snapshot.params.titulo;
  }

  compra() {
    //dummy
    this.router.navigate(['/productos']);
  }

  validar(obj: any, nombre: string): boolean {
    var retorna = true;
    if (obj == undefined || obj == "") {
      this.errorText = 'Por favor complete ' + nombre;
      this.errorLabel = true;
      setTimeout(() => {
        this.errorLabel = false;
      }, 2500);

      retorna = false
    }
    return retorna;
  }

}
