import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiciosService } from '../persistencia/servicios.service';
import { productosDTO } from '../persistencia/usuariosDTO.model';

@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.component.html',
  styleUrls: ['./nuevo.component.css']
})
export class NuevoComponent implements OnInit {

  errorLabel = false;
  successLabel = false;
  errorText: string;
  successText: string;

  producto: productosDTO = new productosDTO();

  constructor(private servicios: ServiciosService, private router: Router) { }

  ngOnInit(): void {
  }

  guardar() {
    if (this.validar(this.producto.titulo, "El titulo del producto")
      && this.validar(this.producto.descripcion, "La descripcion del producto")
      && this.validar(this.producto.imagen, "La imagen del producto")
      && this.validar(this.producto.estado, "El estado del producto")
    ) {
      this.servicios.setProducto(this.producto).subscribe(result => {
        this.producto.usuariocreacion = "ccaycedo";
        this.router.navigate(['/productos']);
        console.log(result);
      },
        error => {
          console.log(error);
          this.errorText = 'Error en [setProducto]';
          this.errorLabel = true;
          setTimeout(() => {
            this.errorLabel = false;
          }, 2500);
        }
      );

    }
  }

  validar(obj: any, nombre: string): boolean {
    var retorna = true;
    if (obj == undefined || obj == "") {
      this.errorText = 'Por favor complete ' + nombre;
      this.errorLabel = true;
      setTimeout(() => {
        this.errorLabel = false;
      }, 2500);

      retorna = false
    }
    return retorna;
  }

}
