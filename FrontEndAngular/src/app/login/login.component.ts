import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ServiciosService } from '../persistencia/servicios.service';
import { usuariosDTO } from '../persistencia/usuariosDTO.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  errorLabel = false;
  successLabel = false;
  errorText: string;
  successText: string;
  usuarios: usuariosDTO = new usuariosDTO();

  constructor(private servicios: ServiciosService, private router: Router) { }

  ngOnInit(): void {
  }

  autenticar() {

    if (this.validar(this.usuarios.login, "El login")
      && this.validar(this.usuarios.clave, "La clave")) {

      console.log(this.usuarios);
      this.servicios.autenticarUsuario(this.usuarios).subscribe(result => {

        console.log(result);
        if (result != null) {
          this.router.navigate(['/productos']);
        } else {
          this.errorText = 'Error en [autenticarUsuario]';
          this.errorLabel = true;
          setTimeout(() => {
            this.errorLabel = false;
          }, 2500);
        }
      },
        error => {
          console.log(error);
          this.errorText = 'Error en [autenticar]';
          this.errorLabel = true;
          setTimeout(() => {
            this.errorLabel = false;
          }, 2500);
        }
      );
    }


  }

  validar(obj: any, nombre: string): boolean {
    var retorna = true;
    if (obj == undefined || obj == "") {
      this.errorText = 'Por favor complete ' + nombre;
      this.errorLabel = true;
      setTimeout(() => {
        this.errorLabel = false;
      }, 2500);

      retorna = false
    }
    return retorna;
  }


}
