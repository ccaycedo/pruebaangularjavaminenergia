package com.example.demo.api.minenergia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Minenergia1Application {

	public static void main(String[] args) {
		SpringApplication.run(Minenergia1Application.class, args);
	}

}
