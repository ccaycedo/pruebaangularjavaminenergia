package com.example.demo.api.minenergia;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping({"/usuarios"})
public class UsuarioController {
	
	@Autowired
	private usuariosDAO dao;
	
	@GetMapping("/getUsuarios")
	public List<usuariosDTO> getUsuarios() {
		return (List<usuariosDTO>) dao.findAll();
	}
	
	@PostMapping("/login")
	public usuariosDTO set(@RequestBody usuariosDTO dto) {		
		return dao.findByLogin(dto.login, dto.clave);
	}
	
	@GetMapping("/test")
	public String test() {
		return "El api responde !";
	}
}
