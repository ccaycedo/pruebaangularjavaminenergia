package com.example.demo.api.minenergia;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.*;

public interface usuariosDAO extends CrudRepository<usuariosDTO,Integer> {
	
	@Query(value = "SELECT * FROM usuarios WHERE login = ?1 and clave=?2", nativeQuery = true)
	public usuariosDTO findByLogin(String login,String clave);

}