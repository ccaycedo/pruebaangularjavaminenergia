package com.example.demo.api.minenergia;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping({"/productos"})
public class productosController {
	
	@Autowired
	private productosDAO dao;
	
	@GetMapping("/get")
	public List<productosDTO> getUsuarios() {
		return (List<productosDTO>) dao.findAll();
	}	
	
	@GetMapping("/test")
	public String test() {
		return "El api responde !";
	}
	
	@PostMapping("/set")
	public productosDTO set(@RequestBody productosDTO dto) {
		return dao.save(dto);
	}

}
