package com.example.demo.api.minenergia;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "usuarios")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class usuariosDTO {
	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int id;
	
	@Column
	public String nombre;
	
	@Column
	public String identificacion;
	
	@Column
	public String direccion;
	
	@Column
	public String telefono;
	
	@Column
	public String email;
	
	@Column
	public String login;
	
	@Column
	public String clave;
	
	@Column
	public String fechacreacion;


}
