package com.example.demo.api.minenergia;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "producto")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class productosDTO {
	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int id;
	
	@Column
	public String titulo;
	
	@Column
	public String descripcion;
	
	@Column
	public String imagen;
	
	@Column
	public int estado;
	
	@Column
	public String usuariocreacion;
	
	@Column
	public String fechacreacion;

}
