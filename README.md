# APLICATIVO OFERTA DE PRODUCTOS #

Es un aplicativo muy sencillo que tiene la parte de login y la parte de creación listado y compra de productos está hecho en angular (FRONTEND) con la plataforma Java como soporte para conexión a base de datos en el backend

### Requerimientos ###

Se requiere una plataforma web que permita generar una interacción directa entre la entidad y los emprendedores (usuarios). Así mismo, que le permita a un usuario registrado (emprendedor), la creación de contenido (como mínimo un título y una descripción del servicio y/o producto a ofrecer) para ser compartido con sus pares (otros emprendedores) y ofrecer sus productos y servicios a potenciales clientes o inversionistas.  

Entregas:  

    Código fuente de la solución Java y Angular y/o React. 
    Video en formato mp4 con la ejecución del programa desarrollado. 
	
### Especificaciones ###

* FrontEnd: Angular
* BackEnd: Java SpringBoot
* Base de datos: Mysql

### Realizado por cristhian caycedo - ccaycedo2@universidadean.edu.co ###